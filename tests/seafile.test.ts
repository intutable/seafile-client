import path from "path"
import fs from "fs"
import { UploadFileData } from "../src/types"
import { Core, EventSystem } from "@intutable/core"
import { createShareLink, createUploadLink, getDirectories, uploadFile } from "../src/requests"
import { checkIfDirectoryExists, createDirectory, getClient } from "../src"
import { CREDENTIALS } from "../src/constants"
import { buildPathFromArray, createDirName, formatShareLink } from "../src/utils"

const localFile = "src/test-data/dummy.pdf"
const bitmap = fs.readFileSync(localFile)
const buffer = Buffer.from(bitmap).toString("base64")

const fileData: UploadFileData = {
    firstName: "Max",
    lastName: "Mustermann",
    emailHash: 10101010,
    file: buffer,
    fileName: "dummy.pdf",
}

let core: Core

beforeAll(async () => {
    core = await Core.create([path.join(__dirname, "../")], new EventSystem(true))
})

afterAll(async () => {
    await core.plugins.closeAll()
})

describe("Test get Client", () => {
    test("can get client", async () => {
        const response = await getClient(CREDENTIALS.username, CREDENTIALS.password)
        expect(response.message).toEqual("getClient succeeded.")
    })
})

describe("Test utils", () => {
    test("can create directory name", async () => {
        const dirName = createDirName(
            fileData.firstName,
            fileData.lastName,
            fileData.emailHash.toString()
        )
        expect(dirName).toEqual("Max-Mustermann-10101010")
    })
    test("can create build path from array", async () => {
        const input_data = ["/", "test", "/", "test", "/", "test.pdf"]
        const repoPath = buildPathFromArray(input_data)
        expect(repoPath).toEqual("test/test/test.pdf")
    })
    test("can format share link", async () => {
        const input_data = "https://test@test@heibox.uni-heidelberg.de/test.pdf"
        const formattedShareLink = formatShareLink(input_data)
        expect(formattedShareLink).toEqual("https://heibox.uni-heidelberg.de/test.pdf")
    })
    test("can create directory", async () => {
        const response = await createDirectory(fileData)
    })
})

describe("Test Plugin Core Requests", () => {
    test("can upload file via request", async () => {
        const response = await core.events.request(uploadFile(fileData))
        expect(response.message).toEqual("uploadFile succeeded.")
    })
    test("can check if directory exists", async () => {
        const response = await checkIfDirectoryExists(CREDENTIALS.directoryPrefix)
        expect(response.message).toEqual("checkIfDirectoryExists succeeded.")
    })
    test("can get directory contents", async () => {
        const response = await core.events.request(getDirectories(CREDENTIALS.directoryPrefix))
        expect(response.message).toEqual("getDirectories succeeded.")
    })
    test("can create upload-link via request", async () => {
        const response = await core.events.request(createUploadLink(fileData))
        expect(response.message).toEqual("createUploadLink succeeded.")
    })
    test("can create share-link via request", async () => {
        const response = await core.events.request(createShareLink(fileData))
        expect(response.message).toEqual("createShareLink succeeded.")
    })
})
