import { CoreRequest, CoreResponse, PluginLoader } from "@intutable/core"
import { CHANNEL } from "./requests"
import { createClient } from "webdav"
import { UploadFileData } from "./types"
import { buildPathFromArray, createDirName, formatShareLink } from "./utils"
import {
    BASE_64_SPLIT_STRING,
    BASE_64_STRING,
    BASE_URL,
    CREDENTIALS,
    DIR_ONLY_REGEX,
    DIR_START_CHAR,
} from "./constants"

// constants needed for HeiBox connection

let core: PluginLoader

/**
 * Init function
 *
 * @param core_
 */
export async function init(core_: PluginLoader) {
    core = core_
    core.listenForRequests(CHANNEL).on(uploadFile.name, uploadFile)
    core.listenForRequests(CHANNEL).on(createShareLink.name, createShareLink)
    core.listenForRequests(CHANNEL).on(createUploadLink.name, createUploadLink)
    core.listenForRequests(CHANNEL).on(getDirectories.name, getDirectories)
}

/**
 * This method returns a client for the given access_token & refresh_token
 *
 * @param username - username
 * @param password - password
 */
export async function getClient(username: string, password: string): Promise<CoreResponse> {
    try {
        const webdav_client = createClient(BASE_URL, {
            // authType: AuthType.Digest,
            username: username,
            password: password,
        })
        return { response: webdav_client, message: "getClient succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "getClient failed." }
    }
}

/**
 * This method lists all directories for a given path
 *
 * @param path - source path
 */
async function getDirectories({ path }: CoreRequest): Promise<CoreResponse> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response

    try {
        const response = await client.getDirectoryContents(path, {
            deep: true,
            glob: DIR_ONLY_REGEX,
        })
        return { response: response, message: "getDirectories succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "getDirectories failed." }
    }
}

/**
 * This method creates a new directory at credentials.directoryPrefix with the name dirName
 *
 * @param fileData - UploadFileData of file to be stored inside directory
 */
export async function createDirectory(
    fileData: UploadFileData
): Promise<{ response: any; message: string }> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response

    if (!CREDENTIALS.directorySuffix) {
        CREDENTIALS.directorySuffix = DIR_START_CHAR
    }

    // create directory name from firstName, lastName and emailHash
    const dirName = createDirName(
        fileData.firstName,
        fileData.lastName,
        fileData.emailHash.toString()
    )

    const repoPath = buildPathFromArray([
        DIR_START_CHAR,
        CREDENTIALS.repositoryName,
        CREDENTIALS.directoryPrefix,
        dirName,
        CREDENTIALS.directorySuffix,
    ])

    try {
        const response = await client.createDirectory(repoPath)
        return { response: response, message: "createDirectory succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "createDirectory failed." }
    }
}

/**
 * This method creates an upload link to the directory credentials.directoryPrefix
 *
 * @param fileData - UploadFileData of file to create Uploadlink for
 */
async function createUploadLink({ fileData }: CoreRequest): Promise<CoreResponse> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response

    // create directory name from firstName, lastName and emailHash
    const dirName = createDirName(
        fileData.firstName,
        fileData.lastName,
        fileData.emailHash.toString()
    )

    if (!CREDENTIALS.directorySuffix) {
        CREDENTIALS.directorySuffix = DIR_START_CHAR
    }

    const repoPath = buildPathFromArray([
        DIR_START_CHAR,
        CREDENTIALS.repositoryName,
        CREDENTIALS.directoryPrefix,
        dirName,
        CREDENTIALS.directorySuffix,
        fileData.fileName,
    ])

    try {
        const response = await client.getFileUploadLink(repoPath)
        return { response: response, message: "createUploadLink succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "createUploadLink failed." }
    }
}

/**
 * This method creates a share (download) link for the file in filePath
 *
 * @param fileData - UploadFileData of file to create Sharelink for
 */
async function createShareLink({ fileData }: CoreRequest): Promise<CoreResponse> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response

    // create directory name from firstName, lastName and emailHash
    const dirName = createDirName(
        fileData.firstName,
        fileData.lastName,
        fileData.emailHash.toString()
    )

    if (!CREDENTIALS.directorySuffix) {
        CREDENTIALS.directorySuffix = DIR_START_CHAR
    }

    const repoPath = buildPathFromArray([
        DIR_START_CHAR,
        CREDENTIALS.repositoryName,
        CREDENTIALS.directoryPrefix,
        dirName,
        CREDENTIALS.directorySuffix,
        fileData.fileName,
    ])

    try {
        const response = await client.getFileDownloadLink(repoPath)
        return { response: response, message: "createShareLink succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "createShareLink failed." }
    }
}

/**
 * This method return true if directoryName exists at directoryPath
 *
 * @param directoryPath - directory to be checked
 */
export async function checkIfDirectoryExists(directoryPath: string): Promise<CoreResponse> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response
    try {
        const response = await client.exists(directoryPath)
        return { response: response, message: "checkIfDirectoryExists succeeded." }
    } catch (error) {
        console.log(error)
        return { response: error, message: "checkIfDirectoryExists failed." }
    }
}

/**
 * This method uploads a pdf file to a directory repoPath
 *
 * @param fileData - UploadFileData of file to be uploaded
 */
async function uploadFile({ fileData }: CoreRequest): Promise<CoreResponse> {
    const client = (await getClient(CREDENTIALS.username, CREDENTIALS.password)).response

    // create directory name from firstName, lastName and emailHash
    const dirName = createDirName(
        fileData.firstName,
        fileData.lastName,
        fileData.emailHash.toString()
    )

    if (!CREDENTIALS.directorySuffix) {
        CREDENTIALS.directorySuffix = DIR_START_CHAR
    }

    // create path from repo to file
    const repoPath = buildPathFromArray([
        DIR_START_CHAR,
        CREDENTIALS.repositoryName,
        CREDENTIALS.directoryPrefix,
        dirName,
        CREDENTIALS.directorySuffix,
        fileData.fileName,
    ])

    // create dedicated user-folder if necessary
    const dirAlreadyExists = await checkIfDirectoryExists(repoPath)
    if (!dirAlreadyExists.response) {
        await createDirectory(fileData)
    }

    // strip of strip for pdf buffer
    const modifiedFile = fileData.file.split(BASE_64_SPLIT_STRING).pop()

    // transform base64 string to buffer
    const buffer = Buffer.from(modifiedFile, BASE_64_STRING)

    try {
        await client.putFileContents(repoPath, buffer)
        let link = await client.getFileDownloadLink(repoPath)
        link = formatShareLink(link)
        return {
            response: true,
            link: link,
            message: "uploadFile succeeded.",
        }
    } catch (error) {
        console.log(error)
        return { response: false, link: null, message: "uploadFile failed." }
    }
}
