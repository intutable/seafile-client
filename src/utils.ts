import axios from "axios"

const UNEXPECTED_ERROR_MSG = "An unexpected error occurred"

// Catch clause variable type annotation must be 'any' or 'unknown' if specified.
export function handleAxiosErrorResponse(error: any) {
    if (axios.isAxiosError(error)) {
        console.log("error message: ", error.message)
        return { response: false, message: error.message }
    } else {
        console.log("unexpected error: ", error)
        return { response: false, message: UNEXPECTED_ERROR_MSG }
    }
}

export function buildPathFromArray(input: string[]) {
    // This method takes an array an return a path
    const result: string[] = []
    input.forEach(item => {
        while (item.includes("/")) {
            item = item.replace("/", "")
        }
        if (!item) {
            return
        }
        result.push(item)
    })
    return result.join("/")
}

export function createDirName(...data: string[]) {
    return data.join("-")
}

export function formatShareLink(url: string) {
    const startIndex = url.indexOf("@")
    const secondIndex = url.indexOf("@", startIndex + 1)
    return "https://" + url.substring(secondIndex + 1)
}
