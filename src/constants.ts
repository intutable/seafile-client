import { HeiboxCredentials } from "./types"

export const DIR_START_CHAR = "/"
export const DIR_ONLY_REGEX = "*/"
export const BASE_64_STRING = "base64"
export const BASE_64_SPLIT_STRING = "base64,"

export const BASE_URL = "https://heibox.uni-heidelberg.de/seafdav"

export const CREDENTIALS: HeiboxCredentials = {
    username: "uni-id@uni-heidelberg.de",
    repositoryName: "test",
    password: "***********+",
    directoryPrefix: "/",
}
