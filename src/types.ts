export interface TokenResponse {
    token: string
}

export interface HeiboxCredentials {
    username: string
    repositoryName: string
    password: string
    directoryPrefix: string
    directorySuffix?: string
}

export interface UploadFileData {
    firstName: string
    lastName: string
    emailHash: number
    file: string //base64 string
    fileName: string
}

export type RepositoryInfo = {
    type: string
    id: string
    owner: string
    owner_name: string
    owner_contact_email: string
    name: string
    mtime: number
    modifier_email: string
    modifier_contact_email: string
    modifier_name: string
    mtime_relative: string
    size: number
    size_formatted: string
    encrypted: boolean
    permission: string
    virtual: boolean
    root: string
    head_commit_id: string
    version: number
    salt: string
}

export type HeiboxFile = {
    modifier_email: string
    size: number
    is_locked: boolean
    lock_owner: any
    lock_time: number
    locked_by_me: boolean
    type: string
    name: string
    id: string
    mtime: number
    permission: string
    modifier_contact_email: string
    modifier_name: string
    starred: boolean
}

export type HeiboxDir = {
    type: string
    name: string
    id: string
    mtime: number
    permission: string
}
