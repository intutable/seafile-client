import { UploadFileData } from "./types"

export const CHANNEL = "seafile-client"

export function getDirectories(path: string) {
    return {
        channel: CHANNEL,
        method: "getDirectories",
        path,
    }
}

export function createUploadLink(fileData: UploadFileData) {
    return {
        channel: CHANNEL,
        method: "createUploadLink",
        fileData,
    }
}

export function createShareLink(fileData: UploadFileData) {
    return {
        channel: CHANNEL,
        method: "createShareLink",
        fileData,
    }
}

export function uploadFile(fileData: UploadFileData) {
    return {
        channel: CHANNEL,
        method: "uploadFile",
        fileData,
    }
}
