## HeiBox-Plugin

The HeiBox-Plugin is a small plugin that connects to HeiBox using a JavaScript [webdav-client](https://github.com/perry-mitchell/webdav-client) It exposes the following main functionalities:

- Upload files to a HeiBox directory
- Create a shareable download link for a HeiBox directory/file
- Create an upload link to a HeiBox directory
- Interact with the HeiBox file system (list directories, check if directories exist)


### Configuration
To use the HeiBox-Plugin, you need to set the configuration details for the HeiBox account you want to use.
You can do this by setting the `CREDENTIALS` variable in the `src/constants.ts` file. For a successful connection, you must specify the **username** (e.g. `uni-id@uni-heidelberg.de`), the HeiBox **repository name** (e.g. `test`), and the webdav **password** (e.g. password). If you don't know where to find or set the webdav password, you can check out this [link](https://www.urz.uni-heidelberg.de/en/newsroom/new-heibox-features-labels-virus-scan-webdav).

###### **Note**
Be sure not to push any confidential credentials into the git history!

### Usage with the dekanat-app-project

A working example-implementation can be found [here](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/blob/feat/f200/heibox-client/gui/src/pages/heibox.tsx). 

To use the HeiBox-Plugin with the dekanat-app-project, you can reference it from the `package.json` file as follows (assuming that the plugin is stored in the parent directory):

```javascript
"dependencies": {
    "@intutable/seafile-client": "../seafile-client",
}
```

Once the plugin is added to the registry, the relative reference won't be necessary anymore.

As with any other plugin for the dekanat-app-project, you need to implement the API endpoints in the next-backend under gui/src/pages/api. An example implementation can be found [here](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f200/heibox-client/gui/src/pages/api/heibox).

Once the endpoints are implemented, create the react-hook for frontend usage. You can find an example [here](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/blob/feat/f200/heibox-client/gui/src/hooks/useHeibox.ts)

Remember to run `$ npm install`.

### Frontend Implementation examples

-   **Page**: [Page](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/blob/feat/f200/heibox-client/gui/src/pages/heibox.tsx)
-   **Hook**: [Hook](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/blob/feat/f200/heibox-client/gui/src/hooks/useHeibox.ts)
-   **Api**: [Api](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f200/heibox-client/gui/src/pages/api/heibox)

Exmple GUI (Files Upload, Share Link generation)

![Example Gui](/assets/seafile_example_gui.png){width=600px}

## Troubleshooting
If you have any question or the plugin does not work as expected, please contact @trostalski or @piksel\_
